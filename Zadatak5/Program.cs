﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD film = new DVD("akcijski film", DVDType.MOVIE, 22.5 );
            VHS crtic = new VHS("crtani film", 12.5);
            Book kuharica = new Book("Kuhanje 1", 25.5);
            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine(film.Accept(buyVisitor));
            Console.WriteLine(crtic.Accept(buyVisitor));
            Console.WriteLine(kuharica.Accept(buyVisitor));
        }
    }
}
