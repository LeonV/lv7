﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            BubbleSort bubbleSort = new BubbleSort();
            CombSort combSort = new CombSort();
            SequentialSort sequentialSort = new SequentialSort();
            double[] array = { 3.2, 5.5, 1.2, 3, 6, 9.2, 1.5, 15, 23, 53.2, 32 };
            
            Console.WriteLine("BubbleSort:");
            NumberSequence numberSequence1 = new NumberSequence(array);
            numberSequence1.SetSortStrategy(bubbleSort);
            Console.WriteLine("Before: \n" +numberSequence1.ToString());
            numberSequence1.Sort();
            Console.WriteLine("After: \n" + numberSequence1.ToString());

            Console.WriteLine("CombSort:");
            NumberSequence numberSequence2 = new NumberSequence(array);
            numberSequence2.SetSortStrategy(combSort);
            Console.WriteLine("Before: \n" + numberSequence2.ToString());
            numberSequence2.Sort();
            Console.WriteLine("After: \n" + numberSequence2.ToString());

            Console.WriteLine("SequentialSort:");
            NumberSequence numberSequence3 = new NumberSequence(array);
            numberSequence3.SetSortStrategy(sequentialSort);
            Console.WriteLine("Before: \n" + numberSequence3.ToString());
            numberSequence3.Sort();
            Console.WriteLine("After: \n" + numberSequence3.ToString());
        }
    }
}
