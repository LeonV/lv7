﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    abstract class  SearchStrategy
    {
        public abstract int Search(double[] array, double Number);

    }
}
