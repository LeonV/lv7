﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 3.2, 5.5, 1.2, 3, 6, 9.2, 1.5, 15, 23, 53.2, 32 };
            NumberSequence numberSequence = new NumberSequence(array);
            LinearSearch linearSearch = new LinearSearch();
            numberSequence.SetSearchStrategy(linearSearch);
            int index = numberSequence.Search(23);
            if (index >= 0)
                Console.WriteLine("Number found at: " + index);
            else
                Console.WriteLine("Number not found");
        }
    }
}
