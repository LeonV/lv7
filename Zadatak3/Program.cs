﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemDataProvider.Attach(consoleLogger);
            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
                
            }
        }
    }
}
