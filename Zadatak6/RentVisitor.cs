﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class RentVisitor : IVisitor
    {
        private const double DVDRentTax = 0.15;
        private const double VHSRentTax = 0.10;
        private const double BookRentTax = 0.05;
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
                return double.NaN;
            else
                return DVDItem.Price * 0.1 * (1 + DVDRentTax);
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * 0.1 * (1 + VHSRentTax);
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price * 0.1 * (1 + BookRentTax);
        }
    }
}
