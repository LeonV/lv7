﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Cart
    {
        private List<IItem> items;

        public Cart() {
            items = new List<IItem>();
        }
        public Cart(List<IItem> Items)
        {
            items = Items;
        }
        public void Add(IItem item) {
            items.Add(item);
        }
        public void Remove(IItem item)
        {
            items.Remove(item);
        }
        public void Clear()
        {
            items.Clear();
        }
        public double Accept(IVisitor visitor) {
            double total = 0;
            foreach (IItem item in items) 
            {
                total += item.Accept(visitor);
            }
            return total;
        }
    }
}
