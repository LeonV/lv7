﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD film = new DVD("akcijski film", DVDType.MOVIE, 22.5 );
            VHS crtic = new VHS("crtani film", 12.5);
            Book kuharica = new Book("Kuhanje 1", 25.5);
            DVD program = new DVD("akcijski film", DVDType.SOFTWARE, 27.5);
            RentVisitor rentVisitor = new RentVisitor();
            Cart cart = new Cart();
            cart.Add(film);
            cart.Add(crtic);
            cart.Add(kuharica);
            cart.Add(program);
            Console.WriteLine(cart.Accept(rentVisitor));
        }
    }
}
